package arangodb

import (
	"time"

	arango "github.com/diegogub/aranGO"
)

const (

	// Maximum attempt connection
	maxAttempt = 10
)

type (

	// Connector that provides information to connect to ArangoDB server.
	Connector interface {

		// GetHost - get host of ArangoDB.
		GetHost() string

		// GetUser - get user of ArangoDB
		GetUser() string

		// GetPassword - get password of ArangoDB.
		GetPassword() string

		// IsLog - Is printing log or not.
		IsLog() bool
	}
)

// NewConnection - instance new session of ArangoDB
func NewConnection(connector Connector) (*arango.Session, error) {
	// Set the session of ArangoDB to zero values
	var conn *arango.Session
	// Set the error of connection to zero values
	var err error

	// Loop until max attempt to connect to ArangoDB server,
	// but once the connection is established, break the loop.
	for i := 1; i <= maxAttempt; i++ {

		// Trying to connecto to ArangoDB server.
		conn, err = arango.Connect(
			connector.GetHost(),
			connector.GetUser(),
			connector.GetPassword(),
			connector.IsLog(),
		)

		// Once connection is established, break the loop
		if err == nil {
			break
		}

		// Give an interval
		time.Sleep(time.Duration(i) * time.Second)
	}

	// After the max attempt connection is reached,
	// and we still can not established connection to ArangoDB server,
	// return the error.
	if err != nil {
		return nil, err
	}

	// Successfully established connection to ArangoDB server
	return conn, nil
}
