package query

import "gitlab.com/didiyudha/auth/modules/user/model"

const (
	databaseName = "auth"
)

type (

	// User query.
	User interface {

		// Save an user
		Save(usr *model.User) error

		// FindByUsernameAndPassword - find user by username and password
		FindByUsernameAndPassword(username, password string) (*model.User, error)
	}
)
