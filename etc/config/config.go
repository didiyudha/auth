package config

import (
	"github.com/joho/godotenv"
)

func init() {
	godotenv.Load()
}

type (

	// Configuration of system.
	Configuration struct {
		Arango *Arango
	}
)

// New configuration instantiation.
func New() *Configuration {
	return &Configuration{
		Arango: &Arango{},
	}
}
