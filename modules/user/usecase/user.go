package usecase

import (
	"gitlab.com/didiyudha/auth/modules/user/model"
	"gitlab.com/didiyudha/auth/modules/user/query"
)

type (

	// User usecase implementation.
	userUsecaseImpl struct {
		UserQuery query.User
	}
)

// Authenticate user implementation.
func (impl *userUsecaseImpl) Authenticate(username, password string) (*model.User, error) {
	return impl.UserQuery.FindByUsernameAndPassword(username, password)
}

// Register user implementation.
func (impl *userUsecaseImpl) Register(user *model.User) error {
	return impl.UserQuery.Save(user)
}
