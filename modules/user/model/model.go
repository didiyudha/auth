package model

import (
	arango "github.com/diegogub/aranGO"
)

type (

	// User model represents user document in MongoDB
	User struct {
		arango.Document
		UserID    uint64
		Username  string
		Password  string
		Token     string
		IsLogedIn bool
	}
)
