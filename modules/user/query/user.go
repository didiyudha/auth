package query

import (
	"fmt"

	arango "github.com/diegogub/aranGO"
	"gitlab.com/didiyudha/auth/modules/user/model"
)

const (
	userCollectionName = "users"
)

type (

	// User query implementation
	userQueryImpl struct {
		ArangoSession *arango.Session
	}
)

func (impl *userQueryImpl) Save(usr *model.User) error {
	return impl.
		ArangoSession.DB(databaseName).
		Col(userCollectionName).
		Save(usr)
}

func (impl *userQueryImpl) FindByUsernameAndPassword(username, password string) (*model.User, error) {
	var user model.User
	q := fmt.Sprintf(`FOR u IN %s 
			FILTER u.username = %s && u.password = %s 
			RETURN u`, userCollectionName, username, password)

	arangoQuery := arango.NewQuery(q)
	cursor, err := impl.
		ArangoSession.
		DB(databaseName).
		Execute(arangoQuery)
	if err != nil {
		return nil, err
	}
	cursor.FetchOne(&user)
	return &user, nil
}
