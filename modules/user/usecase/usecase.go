package usecase

import "gitlab.com/didiyudha/auth/modules/user/model"

type (

	// Authenticator user
	Authenticator interface {
		Authenticate(username, password string) (*model.User, error)
	}

	// Registrator user
	Registrator interface {
		Register(user *model.User) error
	}

	// User usecase
	User interface {
		Authenticator
		Registrator
	}
)
