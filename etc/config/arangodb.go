package config

import (
	"os"
	"strings"
)

type (

	// Arango configuration.
	Arango struct{}
)

// GetHost - get host of ArangoDB server.
func (*Arango) GetHost() string { return os.Getenv("ARANGO_HOST") }

// GetUser - get user of ArangoDB.
func (*Arango) GetUser() string { return os.Getenv("ARANGO_USER") }

// GetPassword - get password of user of ArangoDB.
func (*Arango) GetPassword() string { return os.Getenv("ARANGO_USER") }

// IsLog - get running environment, log only print when system running on
// development environment.
func (*Arango) IsLog() bool {
	env := os.Getenv("ENV")
	if strings.ToUpper(env) == "DEV" {
		return true
	}
	return false
}
